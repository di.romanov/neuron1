#!/usr/bin/env python3
from Neuron import Neuron

class Sinaps:
    
    #neuron that get us data
    neuronIn = Neuron("", 0)
    #neuron that take our data
    neuronOut = Neuron("", 0)

    name = ""

    __value = 0.0

    #grad = 0.0

    deltaValueLast = 0.0

    def __init__(self, name, neuronIn, neuronOut, value = 0):
        self.name = name
        self.neuronIn = neuronIn
        self.neuronOut = neuronOut
        self.__value = value

    def getValue(self):
        return self.__value

    def setValue(self, value):
        self.__value = value