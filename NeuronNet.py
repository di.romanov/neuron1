#!/usr/bin/env python3
import math
import time
from Neuron import Neuron
from Sinaps import Sinaps
from Logger import Logger

class NeuronNet:

    logger = Logger()

    sinapsArray = []
    neuronArray = []

    epsilon = 0.3#скорость обучения
    alfa = 0.5 #момент

    #########CASH############

    __outputNeuronCash = Neuron("CASH", 0, -1)
    __rowsCash = 0



    def setInVal(self, neuronName, neuronValue):
        """Задаем значения входных нейронов"""
        for neuron in self.neuronArray:
            if(neuronName == neuron.name):
                self.logger.log("Neuron ", neuron.name, " set value: ", neuronValue)
                neuron.setValue(neuronValue)
                return
        self.logger.log("Neuron ", neuron.name, "not found.")

    def runNeuron(self):
        """Запустить нейронную сеть"""
        rows = self.__countRows()
        #self.logger.log("RUN NEURON")
        for i in range(1, rows):
            #self.logger.log("Counting row: ", i)
            neurons = self.__getNeuronsFromRow(i)
            for neuron in neurons:
                inSinapses = self.__getInSinapses(neuron)
                value  = self.__countNeuronValue(inSinapses)
                neuron.setValue(value)
                #self.logger.log("Neuron ", neuron.name, "value: ", value)
                if i == rows-1:
                    return value

    def teachNeuronNetItirarion(self, outIdeal, outActual):
        """Обучить нейронную сеть."""
        outNeuron = self.__getOutputNeuron()
        outNeuron.delta = self.__sigmaOutput(outIdeal, outActual)
        #self.logger.log("TEACH NEURON NET ITIRATION")
        #self.logger.log("Out neuron (", outNeuron.name, ") delta: ", outNeuron.delta)
        self.__teachInNeuronSinapses(outNeuron)
        for i in range(0, (outNeuron.row -1)):
            #self.logger.log("Row: ",i)
            neurons = self.__getNeuronsFromRow((outNeuron.row -1) - i)
            for neuron in neurons:
                #self.logger.log("Neuron sinaps study ", neuron.name)
                self.__teachInNeuronSinapses(neuron)


    def __teachInNeuronSinapses(self, neuron):
        """Обучить все входящие синапсы нейрона"""
        neuronSinapses = self.__getInSinapses(neuron)
        for sinaps in neuronSinapses:
            n = sinaps.neuronIn
            n.delta = self.__sigmaHidden(n)
            grad = n.getValue() * sinaps.neuronOut.delta
            dw = self.__mor(grad, sinaps.deltaValueLast)
            sinaps.deltaValueLast = dw
            sinaps.setValue(sinaps.getValue() + dw)
            #self.logger.log("Sinaps (", sinaps.name, ")", " grad: ", grad, " dw: ", dw, " value: ", sinaps.getValue())    





        
    ################МАТЕМАТИКА ОБУЧЕНИЕ НЕЙРОСЕТИ################
    def __fSigmoid(self, out):
        """Производная функции активации"""
        return (1 - out) * out

    def __fTangh(self, out):
        """Производная функции активации"""
        return 1 - out * out

    def __sigmaOutput(self, outIdeal, outActual):
        """Расчет дельты для выходного нейрона"""
        return (outIdeal - outActual) * self.__fSigmoid(outActual)

    def __sigmaHidden(self,neuron):
        """Расчет дельты для скрытого нейрона"""
        outSinapses = self.__getOutSinapses(neuron)
        fSigmoid = self.__fSigmoid(neuron.getValue())
        sum = 0
        for sinaps in outSinapses:
            sum = sum + sinaps.getValue() * sinaps.neuronOut.delta
        return sum * fSigmoid

    def __grad(self, a, b):
        return a * b

    def __mor(self, grad, dw):
        """"Функция МОР"""
        return self.epsilon * grad + self.alfa * dw

    


    ################МАТЕМАТИКА РАСЧЕТ ПО НЕЙРОСЕТИ################
    def __countNeuronValue(self, inSinapses):
        """Расчет значения нейрона на основе входных синапсов"""
        h1 = 0.0
        for sinaps in inSinapses:
             h1 = h1 + sinaps.getValue() * sinaps.neuronIn.getValue()
             ##print("Sinaps value: ",sinaps.getValue(),
             # "Neuron value:", sinaps.neuronIn.getValue(),
              #"Neuron name:", sinaps.neuronIn.name)
        hOutpit = self.__activationFunction(h1)
        ##print ("h1: ", h1, "h2: ", hOutpit)
        return hOutpit

    def __activationFunction(self, inValue):
        """Функция активации"""
        return 1.0/(1.0 + math.exp(-inValue))


    ################РЯДЫ НЕЙРОСЕТИ НЕЙРОСЕТИ################
    def __countRows(self):
        """Получить количество рядов в нейросети"""
        if self.__rowsCash == 0:
            rows = 0
            for neuron in self.neuronArray:
                if(rows < neuron.row):
                    rows = neuron.row
            self.__rowsCash = rows + 1
        return self.__rowsCash

    def __getNeuronsFromRow(self, rowNumber):
        """Получить нейроны находящиеся в выбраного ряда"""
        neurons = []
        for neuron in self.neuronArray:
            if(rowNumber == neuron.row):
                neurons.append(neuron)
        return neurons

    def __getOutputNeuron(self):
        """Получить выходной нейрон"""
        if(self.__outputNeuronCash.row == 0):
            rows = self.__countRows()
            self.__outputNeuronCash = self.__getNeuronsFromRow(rows-1)[0]
        return self.__outputNeuronCash


    ########РАБОТА С МАССИВОМ СИНАПСОВ#########
    def __getInSinapses(self, neuron):
        """Получить входные синапсы нейрона"""
        sinapses = []
        for sinaps in self.sinapsArray:
            if sinaps.neuronOut.name == neuron.name:
                sinapses.append(sinaps)
        return sinapses

    def __getOutSinapses(self, neuron):
        """Получить выходные синапсы нейрона"""
        sinapses = []
        for sinaps in self.sinapsArray:
            if sinaps.neuronIn.name == neuron.name:
                sinapses.append(sinaps)
        return sinapses


 # Класс строки таблицы куда записывается результат серии итераций.
 # a, b - входные значения
 # с - ожидаемое выходное значение
 # result - реальное выходное значение
class ResultTableLine:
    """Строка результата ответа для таблицы бинарных операций"""
    a = 0.0
    b = 0.0
    c = 0.0
    result = 0.0
    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c
#Таблица для записи результатов последней итерации.
xorResultTable = [ResultTableLine(0, 0, 0),
                    ResultTableLine(0, 1, 1),
                    ResultTableLine(1, 0, 1),
                    ResultTableLine(1, 1, 0)]

#допустимая погрешность, как только достигаем ее считаем нейронку обученой   
delta = 0.01                

def main():

    #добавляем нейроны в объект нейросети
    i1 = Neuron("I1", 0)
    i2 = Neuron("I2", 0)
    h1 = Neuron("H1", 1)
    h2 = Neuron("H2", 1)
    o1 = Neuron("O1", 2)

    #добавляем синапсы в объект нейросети
    w1 = Sinaps("w1",i1, h1, 0.45)
    w2 = Sinaps("w2",i1, h2, 0.78)
    w3 = Sinaps("w3",i2, h1, -0.12)
    w4 = Sinaps("w4",i2, h2, 0.13)
    w5 = Sinaps("w5",h1, o1, 1.5)
    w6 = Sinaps("w6",h2, o1, -2.3)

    
    nn = NeuronNet()
    #создаем таблицу записи результатов
    nn.sinapsArray = [w1, w2, w3, w4, w5, w6]
    nn.neuronArray = [i1, i2, h1, h2, o1]

    #первый тренировачный сет вне замкнутого цикла
    #цель сета сравнить результаты вычислений с ожидаемыми взятыми из статьи
    #https://habr.com/ru/post/312450/
    print("TEST FROM EXAMPLE")
    #задаем входные параметры
    i1.setValue(1.0)
    i2.setValue(0.0)
    #получаем результат работы сети после одного сета
    ans = nn.runNeuron()
    print("First ans: ", ans)
    #проводим обучение
    nn.teachNeuronNetItirarion(1, ans)
    #получаем результаты работы после обучения
    ans = nn.runNeuron()
    print("Second ans: ", ans)

    #запускаем непрерывное обучение в цикле
    print("RUNING STUDY")
    nn.logger.off()
    i = 0
    #время запуска нейронки (для измерения времени n итераций)
    previous = int(round(time.time() * 1000))
    #ремя запуска нейронки (для измерения времени нахожденгеия результата)
    startTime = int(round(time.time() * 1000))
    while(True): 
        if i%1000000 == 0 and i!=0:
            nn.logger.on()
        for r in xorResultTable:
            i1.setValue(r.a)
            i2.setValue(r.b)
            ans = nn.runNeuron()
            nn.teachNeuronNetItirarion(r.c, ans)
            r.result = ans
        #каждый 100000 сет выводим результы работы в консоль
        if i%100000 == 0:
            current = int(round(time.time() * 1000))
            delta = current - previous
            previous = current
            print("Iteration ", i, 
            " W: ", xorResultTable[0].c, " H: ", xorResultTable[0].result,
            " W: ", xorResultTable[1].c, " H: ", xorResultTable[1].result, 
            " W: ", xorResultTable[2].c, " H: ", xorResultTable[2].result,
            " W: ", xorResultTable[3].c, " H: ", xorResultTable[3].result,
            " deltaTime: ", delta)
            nn.logger.off()
            if offset():
                break
        i = i + 1
    stopTime = int(round(time.time() * 1000))
    deltaMs = stopTime - startTime
    deltaS = deltaMs/1000
    print("Study time [s]: ", deltaS, "\nStudy time [ms]: ", deltaMs)



def offset():
    """Проверят обучилась ли нейронка"""
    for r in xorResultTable:
        if(r.c < r.result -delta or r.c> r.result + delta):
            return False
    return True





if __name__ == '__main__':  # Если мы запускаем файл напрямую, а не импортируем
    main()  # то запускаем функцию main()


        


    





