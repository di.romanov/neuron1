#!/usr/bin/env python3

class Neuron:

    __value = 0.0

    delta = 0.0

    name = ""

    #ряд  в нейросети
    row = 0

    def __init__(self, name, row, value = 0):
        self.name = name
        self.__value = value
        self.row = row

    def getValue(self):
        return self.__value

    def setValue(self, value):
        self.__value = value