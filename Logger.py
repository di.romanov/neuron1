#!/usr/bin/env python3

class Logger:
    
    __enable = True

    def on(self):
        self.__enable = True

    def off(self):
        self.__enable = False

    def log(self, *args):
        if(self.__enable == True):
            print(*args)
